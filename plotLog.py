import sys
from pathlib import Path


logfile = sys.argv[1]

# Get logfile name without extension
logfile_name = Path(logfile).stem
# Output kml with same filename
kmlfile = logfile_name + '.kml'

# Read logfile
with open(logfile, 'r') as f:
    lines = f.readlines()

# Parse lines like Entry: 0 Time: 56:13:11 Latitude: 36.1035 Longitude: -78.33153 for lat,lon and write to kml
with open(kmlfile, 'w') as f:
    f.write('<?xml version="1.0" encoding="UTF-8"?>\n')
    f.write('<kml xmlns="http://www.opengis.net/kml/2.2">\n')
    f.write('<Document>\n')
    f.write('<name>' + logfile_name + '</name>\n')
    f.write('<description>GPS log converted to KML</description>\n')
    f.write('<Style id="yellowLineGreenPoly">\n')
    f.write('<LineStyle>\n')
    f.write('<color>7f00ffff</color>\n')
    f.write('<width>4</width>\n')
    f.write('</LineStyle>\n')
    f.write('</Style>\n')
    f.write('<Placemark>\n')
    f.write('<styleUrl>#yellowLineGreenPoly</styleUrl>\n')
    f.write('<LineString>\n')
    f.write('<extrude>1</extrude>\n')
    f.write('<tessellate>1</tessellate>\n')
    f.write('<altitudeMode>clampToGround</altitudeMode>\n')
    f.write('<coordinates>\n')
    for l in lines:
        if 'Latitude' in l:
            lat = l.split('Latitude: ')[1].split(' Longitude: ')[0]
            lon = l.split('Longitude: ')[1].strip()
            f.write(lon + ',' + lat + ',0\n')
    f.write('</coordinates>\n')
    f.write('</LineString>\n')
    f.write('</Placemark>\n')
    f.write('</Document>\n')
    f.write('</kml>\n')
