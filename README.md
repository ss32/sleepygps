# ESP32 GPS Logger

BOM:
 * ESP32 WROVER
 * BN160 GPS antenna
 * I2C 32kB FRAM module
 * Two momentary buttons

 ## Functions

### Default Mode 
 Logs GPS at a prescribed rate to memory.  When the memory has filled up the log circles back to the beginning and starts overwriting earlier entries.



### Dump Logs
#### Note: Button presses do *not* wake the ESP from deep sleep. The best way to trigger a log dump or memory reset is to press and hold until you see the LED light up.
Pressing the memory dump button will dump all logs to the serial terminal at 115200 baud and the onboard LED will blink to let you know that logs are being dumped to serial.  The current format of the log is 

 ```
 Entry: N Time: HH:MM:SS Latitude XX.YYYY Longitude XX.YYYY
 ```

Where `Time` is UTC and `Lat/Lon` are decimal degrees. 

**Known Issue**: Operation of this tracker was designed with the Western hempisphere in mind at longitudes < 100<sup>o</sup> West.  Instead of using a parity bit to indicate E/W values, a value greater than 100 indicates a Westerly longitude value and 256 is subtracted from the raw value to reconstruct the original reading. A future version may account for this, but for now don't use this too far West of Dallas.

![logs](logdump.jpg)

### Clear Memory
Pressing the memory clear button will reset all working memory and start logging at the beginning.  While the memory is being cleared the onboard LED will remain lit and `CLEARING FRAM` will print to a serial monitor.

### Plot Logs

Run `python3 plotLog.py <SERIAL_LOG.log>` to generate a KML named the same as `<SERIAL_LOG.log>`.  *Note*: The file name of the log does not matter, just pass it as the argument to the python script.
