#include <HardwareSerial.h>
#include <esp_sleep.h>
#include "TinyGPS++.h"
#include "Adafruit_FRAM_I2C.h"

static const uint32_t GPSBaud = 9600;

#define TIME_TO_SLEEP 45       /* Time ESP32 will go to sleep (in seconds) */
#define uS_TO_S_FACTOR 1000000 /* Conversion factor for micro seconds to seconds */
#define FRAM_ADDR 0x50
#define FRAM_SIZE 32768
#define LOG_ENTRY_WIDTH 14
#define MEM_CLEAR_BUTTON_PIN 23
#define MEM_DUMP_BUTTON_PIN 25
#define LED_PIN 2
#define READ_OFFSET 2
#define SYNC_BYTE 0xff
#define DEBUG false
struct parts
{
  int whole{0};
  uint tenths{0};
  uint hundredths{0};
  uint thousandths{0};
  uint ten_thousands{0};
};
u_int8_t hour = 0;
u_int8_t minute = 0;
u_int8_t second = 0;
float latitude = 0;
float longitude = 0;

parts lat_p;
parts lon_p;
HardwareSerial SerialGPS(1);
TinyGPSPlus gps;
int inByte = 0;
int retval = 0;
uint16_t entry_index = 0;
Adafruit_FRAM_I2C fram = Adafruit_FRAM_I2C();

// Given a float, return a truncated float with the specified number of decimal places.
float truncate(float val, byte dec)
{
  float x = val * pow(10, dec);
  float y = round(x);
  float z = x - y;
  if ((int)z == 5)
  {
    y++;
  }
  else
  {
  }
  x = y / pow(10, dec);
  return x;
}

// Attempts to autmatically search for the first log entry based on the sync byte
uint8_t findLogEntry()
{
  uint8_t val{0};
  for (uint8_t i = 0; i < 3 * LOG_ENTRY_WIDTH; i++)
  {
    fram.read(i, &val, 1);
    if (val == SYNC_BYTE)
    {
      // Return the index of the first byte of the log entry
      // Offset by 1 because start point is inclusive
      return i - LOG_ENTRY_WIDTH + 1;
    }
  }
  // If we don't find a valid log entry, return what should be the start of the log
  return 2;
}

// Given a float with 4 decimal places, returns 4 ints
// representing the whole number, the tenths, the hundredths,
// and the thousandths place.
void cleanReadings(float val, parts &p)
{
  float val_rounded = truncate(val, 4);
  p.whole = int(val_rounded);
  if (val_rounded < 0)
  {
    val_rounded = val_rounded * -1;
  }
  p.tenths = int((val_rounded - abs(p.whole)) * 10);
  p.hundredths = int((val_rounded - abs(p.whole) - (float)p.tenths / 10) * 100);
  p.thousandths = int((val_rounded - abs(p.whole) - (float)p.tenths / 10 - (float)p.hundredths / 100) * 1000);
  p.ten_thousands = int((val_rounded - abs(p.whole) - (float)p.tenths / 10 - (float)p.hundredths / 100 - (float)p.thousandths / 1000) * 10000);
}

// Clears the FRAM up to the last valid log entry
void clearMemory()
{
  digitalWrite(LED_PIN, HIGH);
  Serial.println("Clearing FRAM");
  fram.write(0x0, 0x00);
  fram.write(0x1, 0x01);
  for (int i = 2; i < FRAM_SIZE; i++)
  {
    fram.write(i, 0x00);
    i++;
    if (i > FRAM_SIZE){
      break;
    }
  }
  entry_index = 1;
  digitalWrite(LED_PIN, LOW);
}

// Dumps the FRAM memory to the serial monitor
// Prints out the time, latitude, and longitude of each log entry
// and tells the user if an entry is invalid based on the sync byte
void dumpMemory()
{
  uint8_t buff[2];
  fram.read(0x0, buff, 2);
  uint16_t log_entries = (buff[0] << 8 | buff[1]) / LOG_ENTRY_WIDTH;
  Serial.println("Number of entries: " + String(log_entries));
  const uint8_t start_point = findLogEntry();
  for (uint16_t a = 0; a < log_entries; a++)
  {
    digitalWrite(LED_PIN, HIGH);
    uint8_t buff[LOG_ENTRY_WIDTH];
    uint idx_start{0};
    uint idx_end{0};
    idx_start = a * LOG_ENTRY_WIDTH + start_point;
    idx_end = idx_start + LOG_ENTRY_WIDTH;
    fram.read(idx_start, buff, LOG_ENTRY_WIDTH);
    if (DEBUG)
    {
      Serial.println("Reading from " + String(idx_start) + " to " + String(idx_end));
    }
    hour = buff[0];
    minute = buff[1];
    second = buff[2];
    lat_p.whole = buff[3];
    lat_p.tenths = buff[4];
    lat_p.hundredths = buff[5];
    lat_p.thousandths = buff[6];
    lat_p.ten_thousands = buff[7];
    lon_p.whole = buff[8];
    if (lon_p.whole > 100)
    {
      lon_p.whole = lon_p.whole - 256;
    }
    lon_p.tenths = buff[9];
    lon_p.hundredths = buff[10];
    lon_p.thousandths = buff[11];
    lon_p.ten_thousands = buff[12];
    if (buff[13] != SYNC_BYTE)
    {
      Serial.println("Invalid entry");
    }
    digitalWrite(LED_PIN, LOW);
    Serial.print("Entry: " + String(a) + " ");
    Serial.print("Time: " + String(hour) + ":" + String(minute) + ":" + String(second) + " ");
    Serial.print("Latitude: " + String(lat_p.whole) + "." + String(lat_p.tenths) + String(lat_p.hundredths) + String(lat_p.thousandths) + String(lat_p.ten_thousands) + " ");
    Serial.print("Longitude: " + String(lon_p.whole) + "." + String(lon_p.tenths) + String(lon_p.hundredths) + String(lon_p.thousandths) + String(lon_p.ten_thousands) + " ");
    Serial.println(" ");
    delay(25);
  }
}

// Logs the GPS data to the FRAM for storage
void logGps()
{
  while (SerialGPS.available() > 0)
  {
    inByte = SerialGPS.read();
    retval = gps.encode(inByte);
    if (retval)
    {
      hour = gps.time.hour();
      minute = gps.time.minute();
      second = gps.time.second();
      latitude = gps.location.lat();
      longitude = gps.location.lng();
    }
  }
  if (latitude != 0.0 && longitude != 0.0)
  {
    // Add 1 to the entry index because we logged the inclusive last index
    // I dk this feels redundant because I add 1 later but also there doesn't 
    // seem to be a way to account for it while keeping the code easy to read
    entry_index += 1;
    if (DEBUG)
    {
      Serial.println("Raw Latitude: " + String(latitude, 6));
      Serial.println("Raw Longitude: " + String(longitude, 6));
    }
    // Call cleanReadings() to get the whole number, tenths, hundredths, and thousandths
    // of the latitude and longitude.
    cleanReadings(latitude, lat_p);
    cleanReadings(longitude, lon_p);
    if (DEBUG)
    {
      Serial.println("Start index: " + String(entry_index));
      Serial.println("Time: " + String(hour) + ":" + String(minute) + ":" + String(second));
      Serial.println("Latitude: " + String(lat_p.whole) + "." + String(lat_p.tenths) + String(lat_p.hundredths) + String(lat_p.thousandths) + String(lat_p.ten_thousands));
      Serial.println("Longitude: " + String(lon_p.whole) + "." + String(lon_p.tenths) + String(lon_p.hundredths) + String(lon_p.thousandths) + String(lon_p.ten_thousands));
    }
    fram.write(entry_index, hour);
    fram.write(entry_index + 1, minute);
    fram.write(entry_index + 2, second);
    fram.write(entry_index + 3, lat_p.whole);
    fram.write(entry_index + 4, lat_p.tenths);
    fram.write(entry_index + 5, lat_p.hundredths);
    fram.write(entry_index + 6, lat_p.thousandths);
    fram.write(entry_index + 7, lat_p.ten_thousands);
    fram.write(entry_index + 8, lon_p.whole);
    fram.write(entry_index + 9, lon_p.tenths);
    fram.write(entry_index + 10, lon_p.hundredths);
    fram.write(entry_index + 11, lon_p.thousandths);
    fram.write(entry_index + 12, lon_p.ten_thousands);
    fram.write(entry_index + 13, SYNC_BYTE);
    // Subtract 1 because the index is inclusive
    entry_index += LOG_ENTRY_WIDTH - 1;
    if (entry_index >= FRAM_SIZE)
    {
      Serial.println("Calculated index: " + String(entry_index) + " is too big for " + String(FRAM_SIZE / LOG_ENTRY_WIDTH) + " entries. Resetting to 0.");
      entry_index = READ_OFFSET;
      fram.write(0x0, 0x00);
      fram.write(0x1, 0x01);
    }
    // Record the number of entries in bytes 0 and 1
    uint8_t buff[2];
    buff[0] = entry_index >> 8;
    buff[1] = entry_index & 0xff;
    fram.write(0x0, buff, 2);
    if (DEBUG)
    {
      Serial.println("Finish index: " + String(entry_index));
    }
  }
}

void setup()
{
  pinMode(MEM_CLEAR_BUTTON_PIN, INPUT_PULLUP);
  pinMode(MEM_DUMP_BUTTON_PIN, INPUT_PULLUP);
  pinMode(LED_PIN, OUTPUT);
  Serial.begin(115200);
  // GPS is on UART2
  SerialGPS.begin(9600, SERIAL_8N1, 16, 17);
  delay(1000);
  // Disable all peripherals during hiberation
  esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);
  esp_sleep_pd_config(ESP_PD_DOMAIN_RTC_PERIPH,   ESP_PD_OPTION_OFF);
  esp_sleep_pd_config(ESP_PD_DOMAIN_RTC_SLOW_MEM, ESP_PD_OPTION_OFF);
  esp_sleep_pd_config(ESP_PD_DOMAIN_RTC_FAST_MEM, ESP_PD_OPTION_OFF);
  esp_sleep_pd_config(ESP_PD_DOMAIN_XTAL,         ESP_PD_OPTION_OFF);
  if (fram.begin())
  {
    uint8_t buff[2];
    fram.read(0x0, buff, 2);
    entry_index = (buff[0] << 8 | buff[1]);
    if (DEBUG)
    {
      Serial.println("Found I2C FRAM");
      Serial.println("Next log entry will be: " + String(entry_index / LOG_ENTRY_WIDTH));
    }
  }
  else
  {
    Serial.println("I2C FRAM not identified ... check your connections?\r\n");
    Serial.println("Will continue in case this processor doesn't support repeated start\r\n");
    while (1)
      ;
  }
}

void loop()
{
  int clearButtonState = digitalRead(MEM_CLEAR_BUTTON_PIN);
  if (clearButtonState == 0)
  {
    clearMemory();
  }
  else
  {
    digitalWrite(LED_PIN, LOW);
  }
  int dumpButtonState = digitalRead(MEM_DUMP_BUTTON_PIN);
  if (dumpButtonState == 0)
  {
    dumpMemory();
  }
  logGps();
  Serial.flush();
  esp_deep_sleep_start();
}